class SobelTransformer implements Transformer {
  PImage transform(PImage inImg) {
    return sobel(inImg);
  }
}

final float[][] vKernel = 
  {
  {0, 0, 0}, 
  {1, 0, -1}, 
  {0, 0, 0}
};

final float[][] hKernel = 
  {
  {0, 1, 0}, 
  {0, 0, 0}, 
  {0, -1, 0}
};

PImage sobel(PImage img) {
  PImage result =  createImage(img.width, img.height, ALPHA);
  int N = hKernel.length;
  float sumV, sumH, sum, brightness, buffer[], max;
  max = 0;
  buffer = new float[img.width * img.height];
  for (int x = 2; x < img.width - 2; x++) {
    for (int y = 2; y < img.height - 2; y++) {
      sumV = sumH = 0;

      for (int xPrim = max(x - N / 2, 0); xPrim <= x + N / 2 && xPrim < img.width; xPrim++) {
        for (int yPrim = max(y - N / 2, 0); yPrim <= y + N / 2 && yPrim < img.height; yPrim++) {
          int xArr = N / 2 + (xPrim - x);
          int yArr = N / 2 + (yPrim - y);
          brightness = brightness(img.pixels[yPrim * img.width + xPrim]);
          sumV += brightness * vKernel[yArr][xArr];
          sumH += brightness * hKernel[yArr][xArr];
        }
      }
      sum =  sqrt(sumV  * sumV + sumH * sumH);
      if (max < sum)
        max = sum;
      int xy = y * img.width + x;
      buffer[xy] = sum;
    }
  }
  float percentage = 0.2f; // percentage of max
  for (int i = 0; i < img.width * img.height; i++) {
    boolean cond = buffer[i] > (int)(percentage * max);
    int temp = cond ? 255 : 0;
    result.pixels[i] = color(temp);
  }
  return result;
}