class Pipeline implements Transformer { //<>//
  private final Transformer[] transforms;
  private PImage lastImgSeen = null;
  private PImage cachedResult = null;
  Pipeline(Transformer[] transforms) {
    this.transforms = transforms;
  }
  
  PImage transform(PImage inImg) {
    if (cachedResult != null && lastImgSeen == inImg)
      return cachedResult;
    
    lastImgSeen = inImg;
    PImage res = inImg.copy();
    for (Transformer t: transforms) {
      res = t.transform(res);
    }
    return res;
  }
}