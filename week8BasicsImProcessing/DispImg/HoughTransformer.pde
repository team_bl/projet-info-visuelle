class HoughTransformer implements Transformer {
  PImage transform(PImage inImg) {
    return hough(inImg)._2;
  }
}


float discretizationStepsPhi = 0.06f;
float discretizationStepsR = 2.5f;

Pair<int[], PImage> hough(PImage edgeImg) {

  // dimensions of the accumulator
  int phiDim = (int) (Math.PI / discretizationStepsPhi);
  int rDim = (int) (
    ((edgeImg.width + edgeImg.height) * 2  + 1)/ discretizationStepsR
    );
  // our accumulator (with a 1 pix margin around)
  int[] accumulator = new int[(phiDim + 2) * (rDim + 2)];
  // Fill the accumulator: on edge points (ie, white pixels of the edge
  // image), store all possible (r, phi) pairs describing lines going
  // through the point.
  float phi, r, rAcc;
  for (int y = 0; y < edgeImg.height; y++) {
    for (int x = 0; x < edgeImg.width; x++) {
      // Are we on an edge?
      if (brightness(edgeImg.pixels[y * edgeImg.width + x]) != 0) {
        for (int phiAcc = 0; phiAcc < phiDim; phiAcc++) {
          /*
            --------------------------------------> x
           |
           |
           |
           |
           |                  
           |                   . center of (r,phi) coordinates (which should be 
           |                    \                               center of img)
           |                     \
           |                    r \angle phi
           |                       (x0,y0): a point  
           \/                      respecting the condition that 
           y               brightness(edgeImg.pixels[y * edgeImg.width + x]) != 0        
           */
          phi = phiAcc * discretizationStepsPhi;
          r  = x * cos(phi) + y * sin(phi);
          rAcc = r / discretizationStepsR + (rDim - 1) / 2;
          accumulator[(int)((phiAcc + 1) * (rDim + 2) + rAcc + 1)]++;
        }
      }
    }
  }

  PImage houghImg = createImage(rDim + 2, phiDim + 2, ALPHA);
  for (int i = 0; i < accumulator.length; i++) {
    houghImg.pixels[i] = color(min(255, accumulator[i]));
  }

  houghImg.resize(edgeImg.width, edgeImg.height);
  return new Pair(accumulator, houghImg);
}