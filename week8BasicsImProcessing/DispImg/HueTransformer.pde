class HueTransformer implements Transformer {
  PImage transform(PImage inImg) {
    return part1Hue(inImg);
  }
}

PImage part1Hue(PImage img) {
  PImage result =  createImage(img.width, img.height, RGB);
  scroll1.update(); 
  scroll2.update();
  for (int i = 0; i < img.width * img.height; i++) {
    float pixelHue = hue(img.pixels[i]);
    if (scroll1.getPos() * 255.0 > pixelHue && scroll2.getPos() * 255.0 > pixelHue ||
      scroll1.getPos() * 255.0 < pixelHue && scroll2.getPos() * 255.0 < pixelHue) {
      result.pixels[i] = 0;
    } else {
      result.pixels[i] = img.pixels[i];
    }
  }
  image(result, 0, 0);
  scroll1.display(); 
  scroll2.display();
  
  return result;
}