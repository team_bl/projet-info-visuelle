
class GaussianTransformer implements Transformer {
  final float[][] kernel;
  GaussianTransformer(float[][] kernel) {
    this.kernel = kernel;
  }
  PImage transform(PImage img) {
    return convolute(img, kernel);
  }
}