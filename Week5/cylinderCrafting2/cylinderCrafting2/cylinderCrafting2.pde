float cylinderBaseSize = 50;
float cylinderHeight = 50;
int cylinderResolution = 40;
PShape openCylinder = new PShape();
PShape topCloser = new PShape();
PShape bottomCloser = new PShape();

void settings() {
  size(800, 800, P3D);
}
void setup() {
  float angle;
  float[] x = new float[cylinderResolution + 1];
  float[] y = new float[cylinderResolution + 1];
  //get the x and y position on a circle for all the sides
  for (int i = 0; i < x.length; i++) {
    angle = (TWO_PI / cylinderResolution) * i;
    x[i] = sin(angle) * cylinderBaseSize;
    y[i] = cos(angle) * cylinderBaseSize;
  }

  openCylinder = createShape();
  openCylinder.beginShape(QUAD_STRIP);
  //draw the border of the cylinder
  int opt = 2;
  for (int i = 0; i < x.length; i++) {
    if (opt == 1) {
      openCylinder.vertex(x[i], y[i], 0);
      openCylinder.vertex(x[i], y[i], cylinderHeight);
    } else {
      openCylinder.vertex( y[i], x[i], 0);
      openCylinder.vertex(y[i], x[i], cylinderHeight);
    }
  }
  openCylinder.endShape();

  topCloser = createShape();
  topCloser.beginShape(TRIANGLE_FAN);
  topCloser.vertex(0, 0, cylinderHeight);
  for (int i = 0; i < x.length; i++) {
    topCloser.vertex(x[i], y[i], cylinderHeight);
  }
  topCloser.endShape();

  bottomCloser = createShape();
  bottomCloser.beginShape(TRIANGLE_FAN);
  bottomCloser.vertex(0, 0, 0);
  for (int i = 0; i < x.length; i++) {
    bottomCloser.vertex(x[i], y[i], 0);
  }
  bottomCloser.endShape();
}
void draw() {
  background(255);

  translate(mouseX, mouseY, 0);
  rotateX(PI / 2);
  shape(openCylinder);
  shape(topCloser);
  shape(bottomCloser);
}