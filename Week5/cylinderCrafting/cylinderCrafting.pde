int selectedTriangle = 1;

PShape triangle = new PShape();

void settings() {
  size(400, 400, P3D);
}

void setup() {
  if (selectedTriangle == 2) {
    triangle = createShape();
    triangle.beginShape(TRIANGLES);
      triangle.vertex(0, 0);
      triangle.vertex(50, 0);
      triangle.vertex(50, 50);
    triangle.endShape();
  }
}
void draw() {
  if (selectedTriangle == 1)
    triangle1Draw();
  else 
    triangle2Draw();
}