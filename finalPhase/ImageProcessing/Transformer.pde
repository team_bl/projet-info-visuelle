interface Transformer {
  PImage transform(PImage inImg);
}