import java.util.List;
import java.util.Collections;
import java.util.Comparator;

class CWComparator implements Comparator<PVector> {
        PVector center;
        public CWComparator(PVector center) {
                this.center = center;
        }

        @Override
        public int compare(PVector b, PVector d) {
                if(Math.atan2(b.y-center.y,b.x-center.x)<Math.atan2(d.y-center.y,d.x-center.x))
                        return -1;
                else
                        return 1;
        }
}

static List<PVector> pVectorListCpy(List<PVector> pVectors) {
    List<PVector> listCpy = new ArrayList<PVector>(pVectors.size());
    for (int i=0; i < pVectors.size(); i++) {
      listCpy.set(i, pVectors.get(i).copy());
    }
    
    return listCpy;
}