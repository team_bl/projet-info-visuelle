ArrayList<PVector> hough(PImage edgeImg, PImage originalImg, int nLines) {
  float discretizationStepsPhi = 0.06f;
  float discretizationStepsR = 2.5f;
      
  ArrayList<Integer> bestCandidates = new ArrayList<Integer>();

  // dimensions of the accumulator
  int phiDim = (int) (Math.PI / discretizationStepsPhi);
  int rDim = (int) (((edgeImg.width + edgeImg.height) * 2 + 1) / discretizationStepsR);
  
  //sin cos precomputation
  if (tabSin == null){
    initSinCosArray(phiDim, discretizationStepsPhi, discretizationStepsR);
  }
 
  // our accumulator (with a 1 pix margin around)
  int[] accumulator = new int[(phiDim + 2) * (rDim + 2)];
  // Fill the accumulator: on edge points (ie, white pixels of the edge
  // image), store all possible (r, phi) pairs describing lines going
  // through the point.
  for (int y = 0; y < edgeImg.height; y++) {
    for (int x = 0; x < edgeImg.width; x++) {
    // Are we on an edge?
      if (brightness(edgeImg.pixels[y * edgeImg.width + x]) != 0) {
        for (int phiIndex = 0; phiIndex < phiDim; phiIndex++){
          float phi = phiIndex * discretizationStepsPhi;
          float r = x * cos(phi) + y * sin(phi);
          int rIndex = (int)(r/discretizationStepsR) + (rDim - 1) / 2;;
          accumulator[(int) ((phiIndex + 1) * (rDim + 2)) + rIndex + 1] += 1;
        }
      }
    }
  }
  
  //DISPLAY : only for dev purpose
  PImage houghImg = createImage(rDim + 2, phiDim + 2, ALPHA);
  for (int i = 0; i < accumulator.length; i++) {
    houghImg.pixels[i] = color(min(255, accumulator[i]));
  }
  //houghImg.updatePixels();
  //houghImg.resize(400,400);
  //image(houghImg, 0, 0);
    
  //LOCAL MAXIMA
  // size of the region we search for a local maximum
  int neighbourhood = 10;
  // only search around lines with more that this amount of votes
  // (to be adapted to your image)
  int minVotes = 150;
  for (int accR = 0; accR < rDim; accR++) {
    for (int accPhi = 0; accPhi < phiDim; accPhi++) {
    // compute current index in the accumulator
    int idx = (accPhi + 1) * (rDim + 2) + accR + 1;
    if (accumulator[idx] > minVotes) {
      boolean bestCandidate=true;
      // iterate over the neighbourhood
      for(int dPhi=-neighbourhood/2; dPhi < neighbourhood/2+1; dPhi++) {
        // check we are not outside the image
        if( accPhi+dPhi < 0 || accPhi+dPhi >= phiDim) continue;
        for(int dR=-neighbourhood/2; dR < neighbourhood/2 +1; dR++) {// check we are not outside the image
          if(accR+dR < 0 || accR+dR >= rDim) continue;
          int neighbourIdx = (accPhi + dPhi + 1) * (rDim + 2) + accR + dR + 1;
          if(accumulator[idx] < accumulator[neighbourIdx]) {
            // the current idx is not a local maximum!
            bestCandidate=false;
            break;
          }
        }
        if(!bestCandidate) break;
        }
        if(bestCandidate) {
        // the current idx *is* a local maximum
        bestCandidates.add(idx);
        }
      }
    }
  }
    
  Collections.sort(bestCandidates, new HoughComparator(accumulator));
  int[] filteredAcc = new int[(rDim +2) * (phiDim + 2)];
  ArrayList<PVector> lines = new ArrayList<PVector>();
  
  int nbLines = min(bestCandidates.size(), nLines);
  for (int i = 0; i < nbLines; i++) {
    int idx = bestCandidates.get(i);
    filteredAcc[idx] = accumulator[idx];
  }
  
  //DISPLAY ORIGINAL IMG AǸD THEN...
  image(originalImg, 0, 0);
  
  //...ADD LINES
  for(int idx = 0; idx < filteredAcc.length; idx++) {
    if (filteredAcc[idx] != 0) {
      int accPhi = (int) (idx / (rDim + 2)) - 1;
      int accR = idx - (accPhi + 1) * (rDim + 2) - 1;
      float r = (accR - (rDim - 1) * 0.5f) * discretizationStepsR;
      float phi = accPhi * discretizationStepsPhi;
      lines.add(new PVector(r, phi));
    }
  }
  originalImg.resize(width/3, height);
  houghImg.resize(width/3, height);
  image(originalImg, 0, 0);
  image(houghImg, originalImg.width, 0);
  image(edgeImg, originalImg.width + houghImg.width, 0);
  drawLines(originalImg.width, lines, nLines);
  return lines;
}

ArrayList<PVector> getIntersections(List<PVector> lines) {
  ArrayList<PVector> intersections = new ArrayList<PVector>();
  for (int i = 0; i < lines.size() - 1; i++) {
      PVector line1 = lines.get(i);
    for (int j = i + 1; j < lines.size(); j++) {
      PVector line2 = lines.get(j);
      // compute the intersection and add it to 'intersections'
      float d = cos(line2.y) * sin(line1.y) - cos(line1.y) * sin(line2.y);
      
      float x = (line2.x * sin(line1.y) - line1.x * sin(line2.y))/d;
      float y = (-line2.x * cos(line1.y) + line1.x * cos(line2.y))/d;
      
      intersections.add(new PVector(x, y));
      // draw the intersection
      fill(255, 128, 0);
      ellipse(x, y, 10, 10);
    }
  }
  return intersections;
}

void initSinCosArray(int phiDim, float discretizationStepsPhi, float discretizationStepsR) {
  //sin/cos in array
  tabSin = new float[phiDim];
  tabCos = new float[phiDim];
  
  float ang = 0;
  float inverseR = 1.f / discretizationStepsR;
  
  for (int accPhi = 0; accPhi < phiDim; ang += discretizationStepsPhi, accPhi++){
    tabSin[accPhi] = (float) sin(ang) * inverseR;
    tabCos[accPhi] = (float) cos(ang) * inverseR;
  } 
}

void drawLines(int imgWidth, ArrayList<PVector> lines, int nLines) {
  int nbLines = min(lines.size(), nLines);
  for (int i = 0; i < nbLines; i++) {
    int x0 = 0;
    int y0 = (int) (lines.get(i).x / sin(lines.get(i).y));
    int x1 = (int) (lines.get(i).x / cos(lines.get(i).y));
    int y1 = 0;
    int x2 = imgWidth;
    int y2 = (int) (-cos(lines.get(i).y) / sin(lines.get(i).y) * x2 + lines.get(i).x / sin(lines.get(i).y));
    int y3 = imgWidth;
    int x3 = (int) (-(y3 - lines.get(i).x / sin(lines.get(i).y)) * (sin(lines.get(i).y) / cos(lines.get(i).y)));
    // Finally, plot the lines
    stroke(204,102,0);
    if (y0 > 0) {
      if (x1 > 0) {
        line(x0, y0, x1, y1);
      } else if (y2 > 0) {
        line(x0, y0, x2, y2);
      } else {
        line(x0, y0, x3, y3);
      }
    } else {
      if (x1 > 0) {
        if (y2 > 0) {
          line(x1, y1, x2, y2);
        } else {
          line(x1, y1, x3, y3);
        } 
      }else {
        line(x2, y2, x3, y3);
      }
    }
  }
}