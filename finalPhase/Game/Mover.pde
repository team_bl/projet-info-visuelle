class Mover {
  final float SPHERE_RADIUS = 20.0f;
  final float NORMAL_FORCE = 1f;
  final float FRICTION_COEF = 0.1f;
  final float BOUNCING_CST = 1f;

  PVector location;
  PVector velocity;
  PVector gravity;

  public Mover() {
    location = new PVector(0, -(SPHERE_RADIUS + boxHeight/2.0), 0);
    velocity = new PVector(0, 0, 0);
    gravity = new PVector(0, 0, 0);
  }

  void update() {
    float frictionMagnitude = NORMAL_FORCE * FRICTION_COEF; // supprimer une cst ou jouer avec la masse de la balle?
    PVector friction = velocity.get();
    friction.mult(-1f); //oppose au mvt
    friction.normalize(); //normallise
    friction.mult(frictionMagnitude); //donne le module

    gravity.x = sin(zRotation) * GRAVITY_CST;
    gravity.z = -sin(xRotation) * GRAVITY_CST;

    location.add((velocity.add(gravity)).add(friction));
  }

  void display() {
    pushMatrix();
    translate(location.x, location.y, location.z);
    sphere(SPHERE_RADIUS);
    popMatrix();
  }

  void checkEdges() {
    if (location.x > boxEdges/2.0) {
      velocity.x = -velocity.x;
      location.x = boxEdges/2.0;
      //score
      double velocityMag = velocity.mag();
      lastScore = velocityMag;
      score += velocityMag;
    } else if (location.x < - (boxEdges/2.0)) {
      velocity.x = -velocity.x;
      location.x = - (boxEdges/2.0);
      //score
      double velocityMag = velocity.mag();
      lastScore = velocityMag;
      score += velocityMag;
    }
    if (location.z >= boxEdges/2.0) {
      velocity.z = -velocity.z;
      location.z = boxEdges/2.0;
      //score
      double velocityMag = velocity.mag();
      lastScore = velocityMag;
      score += velocityMag;
    } else if (location.z < - (boxEdges/2.0)) {
      velocity.z = -velocity.z;
      location.z = - (boxEdges/2.0);
      //score
      double velocityMag = velocity.mag();
      lastScore = velocityMag;
      score += velocityMag;
    }
  }

  void checkCylinderCollision() {
    for (PVector v : cylindersPos) {
      PVector distV = distVect(v);
      float dist = distV.mag();
      float minDist = SPHERE_RADIUS + cylinderBaseSize;
      if (dist <= (minDist)) {
        //la boule n est pas dans le cylindre quand elle rebondit
        distV.mult(minDist/dist);
        location.x = v.x - distV.x;
        location.z = v.y - distV.y;

        //rebond       
        PVector normal = new PVector(location.x, 0, location.z);
        normal.sub(v.x, 0, v.y);
        normal.normalize();
        PVector tempVelo = new PVector(velocity.x, 0, velocity.z);
        velocity = velocity.sub(normal.mult(2*(tempVelo.dot(normal))));
        velocity.mult(BOUNCING_CST);
      }
    }
  }

  PVector distVect(PVector v) {
    return new PVector((v.x - location.x), (v.y - location.z));
  }
}