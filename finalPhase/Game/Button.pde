class Button {
  boolean mouseOver, wasPressed;
  float xPos, yPos, buttonWidth, buttonHeight;
  Button(float x, float y, float w, float h) {
    xPos = x; 
    yPos = y;
    buttonWidth = w;
    buttonHeight = h;
    mouseOver = wasPressed = false;
  }
  
  void display() {
    noStroke();
    
    if (mouseOver) 
      fill(102, 102, 102);
    else 
      fill(255, 255, 255);
    
    rect(xPos, yPos, buttonWidth, buttonHeight);
  }
  
  void updateMouseOver() {
    mouseOver = mouseX > xPos && mouseX < xPos+buttonWidth &&
      mouseY > yPos && mouseY < yPos+buttonHeight;
  }
  
  void update() {
    updateMouseOver();
    wasPressed = mouseOver && mousePressed;
  }
}