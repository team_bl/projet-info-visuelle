
class GaussianTransformer implements Transformer {
  final float[][] kernel;
  GaussianTransformer(float[][] kernel) {
    this.kernel = kernel;
  }
  public PImage transform(PImage img, PApplet app) {
    return convolute(img, kernel);
  }
}