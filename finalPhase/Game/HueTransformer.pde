class HueTransformer implements Transformer {
  public PImage transform(PImage inImg, PApplet app) {
    return part1Hue(inImg, app, false);
  }
}


PImage part1Hue(PImage img, PApplet app, boolean displayScrolls) {
  PImage result = createImage(img.width, img.height, RGB);
  int hueMin = 96;
  int hueMax = 136;
  if (displayScrolls) {
    scroll1.update();
    scroll2.update();
    hueMin = (int) scroll1.getPos();
    hueMax = (int) scroll2.getPos();
  }

  //CHOSEN VALUES
  for (int i = 0; i < img.width * img.height; i++) {
    int hue = (int) hue(img.pixels[i]);
    if (hue >= hueMin && hue <= hueMax) {
      result.pixels[i] = img.pixels[i]; //keeps its color
    } else {
      result.pixels[i] = color(0); //set to black
    }
  }
  //DEBUG
  //app.image(result, 0, 0);
  if (displayScrolls) {
    scroll1.display();
    scroll2.display();
  }
  return result;
}

/*
PImage part1Hue(PImage img, PApplet app, boolean displayScrolls) {
  PImage result =  createImage(img.width, img.height, RGB);
  scroll1.update(); 
  scroll2.update();
  for (int i = 0; i < img.width * img.height; i++) {
    float pixelHue = hue(img.pixels[i]);
    if (scroll1.getPos() * 255.0 > pixelHue && scroll2.getPos() * 255.0 > pixelHue ||
      scroll1.getPos() * 255.0 < pixelHue && scroll2.getPos() * 255.0 < pixelHue) {
      result.pixels[i] = 0;
    } else {
      result.pixels[i] = img.pixels[i];
    }
  }
  app.image(result, 0, 0);
  if (displayScrolls) {
    scroll1.display(); 
    scroll2.display();
  }

  return result;
}*/