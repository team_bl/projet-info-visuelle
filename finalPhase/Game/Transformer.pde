interface Transformer {
  PImage transform(PImage inImg, PApplet app);
}