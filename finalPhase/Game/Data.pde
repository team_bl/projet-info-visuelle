final float DATA_HEIGHT = 150.0f;
final float TOP_VIEW_MARGIN = 10.0f;
final float TOP_VIEW_EDGES = DATA_HEIGHT - 2.0f * TOP_VIEW_MARGIN;
final float SCORE_BOARD_HEIGHT = 4/5.0 * DATA_HEIGHT - TOP_VIEW_MARGIN;
final float SCROLL_HEIGHT = DATA_HEIGHT - SCORE_BOARD_HEIGHT - 3 * TOP_VIEW_MARGIN;
final float SPHERE_RADIUS = 20.0f;
final float CIRCLE_RADIUS = 2 * SPHERE_RADIUS/boxEdges * TOP_VIEW_EDGES;
final float CYLINDRE_RADIUS = 2 * cylinderBaseSize/boxEdges * TOP_VIEW_EDGES;
float dataRectXEdge = 5f;
final float dataRectYEdge = 5f;

double score = 0.0;
double lastScore = 0.0;

PGraphics dataBackground; 
PGraphics topView;
PGraphics scoreboard;
PGraphics barChart;

ArrayList<ArrayList<PVector>> scoreRectPositions = new ArrayList<ArrayList<PVector>>();

String scoreText = "";

int counter = 0;

void drawData() {
  setText();

  dataBackground.beginDraw();
  dataBackground.background(#E6E2AF);
  dataBackground.endDraw();

  topView.beginDraw();
  topView.background(#066582);
  addCylinders();
  addMover();
  topView.endDraw();

  scoreboard.beginDraw();
  scoreboard.fill(120, 120, 120);
  scoreboard.stroke(255);
  scoreboard.strokeWeight(1.0);
  scoreboard.background(255, 255, 255);
  scoreboard.line(0, 0, TOP_VIEW_EDGES, 0);
  scoreboard.line(0, 0, 0, TOP_VIEW_EDGES);
  scoreboard.line(0, TOP_VIEW_EDGES -1, TOP_VIEW_EDGES -1, TOP_VIEW_EDGES-1);
  scoreboard.line(TOP_VIEW_EDGES - 1, 0, TOP_VIEW_EDGES -1, TOP_VIEW_EDGES);
  scoreboard.text(scoreText, TOP_VIEW_MARGIN, TOP_VIEW_MARGIN, TOP_VIEW_EDGES, TOP_VIEW_EDGES);
  scoreboard.endDraw();

  barChart.beginDraw();
  barChart.background(#E3E5D5);
  drawScoreRect();
  barChart.endDraw();

  if (counter <= 20) {
    counter++;
  } else {
    counter = 0;
    updateScoreRectPos();
    addScoreRect();
  }
}

void updateScoreRectPos() {
  for (ArrayList<PVector> p : scoreRectPositions) {
    for (PVector pp : p) {
      pp.x -= dataRectXEdge + 2;
    }
  }
  println(scoreRectPositions.size() + " SIZE");
}

void drawScoreRect() {
  barChart.fill(0, 50, 255);
  for (int i = 0; i < scoreRectPositions.size() ; i++) {
    if (scoreRectPositions.get(i).get(0).x < - 2 * dataRectXEdge) {
      scoreRectPositions.remove(i);
    } else {
      for (PVector p : scoreRectPositions.get(i)) {
        barChart.rect(p.x, p.y, dataRectXEdge, dataRectYEdge);
      }
    }
  }
}
  void addScoreRect() {
    int nb = (int) (score / 50.0);
    if (nb > 0) {
      float posX = (width - 4 * TOP_VIEW_MARGIN - 2 * TOP_VIEW_EDGES) - dataRectXEdge - 1;
      float posY = SCORE_BOARD_HEIGHT - dataRectYEdge - 1;
      ArrayList<PVector> rectColonne = new ArrayList<PVector>();
      for (int i = 0; i < nb; i++) {
        rectColonne.add(new PVector(posX, posY));
        posY += - dataRectYEdge - 2;
      }
      scoreRectPositions.add(rectColonne);
    }
  }

  void updateRectSize() {
    float newPos = 2 * scrollBar.getPos() * dataRectYEdge ;
    if (newPos != dataRectXEdge) {
      int size = scoreRectPositions.size();
      for (int i = size -1; i >= 0; i--) {
        for (PVector p : scoreRectPositions.get(i)) {
          p.x += (dataRectXEdge - newPos) * (size - i);
        }
      }
      dataRectXEdge = newPos;
    }
  }

  void addCylinders() {
    PVector positionOnSquare;
    topView.fill(255, 0, 0);
    for (PVector v : cylindersPos) {
      positionOnSquare = convertToSquareCoord(v, false);
      topView.ellipse(positionOnSquare.x, positionOnSquare.y, CYLINDRE_RADIUS, CYLINDRE_RADIUS);
    }
  }

  void addMover() {
    PVector positionOnSquare = convertToSquareCoord(mover.location, true);
    topView.fill(0, 0, 255);
    topView.ellipse(positionOnSquare.x, positionOnSquare.y, CIRCLE_RADIUS, CIRCLE_RADIUS);
  }

  PVector convertToSquareCoord(PVector position, boolean isMover) {
    if (isMover) {
      return new PVector(map(position.x, -boxEdges/2.0f, boxEdges/2.0f, 0f, DATA_HEIGHT-20), 
        map(position.z, -boxEdges/2.0f, boxEdges/2.0f, 0f, DATA_HEIGHT-20));
    } else {
      return new PVector(map(position.x, -boxEdges/2.0f, boxEdges/2.0f, 0f, DATA_HEIGHT-20), 
        map(position.y, -boxEdges/2.0f, boxEdges/2.0f, 0f, DATA_HEIGHT-20));
    }
  }

  void setText() {
    String scoreStr = "Total score\n" + Math.floor(score * 100)/100.0 + "\n\n";
    String velocityStr = "Velocity\n" + Math.floor(mover.velocity.mag() * 100)/100.0 + "\n\n";
    String lastScoreStr = "Last Score\n" + Math.floor(lastScore * 100)/100.0;
    scoreText = scoreStr + velocityStr + lastScoreStr;
  }