class TresholdTransformer implements Transformer {
  String mode = "";
  float tresholdBrightness;
  TresholdTransformer(String mode, float tresholdBrightness) { // either tresholdBinary or tresholdBinaryInverted
    this.mode = mode;
    this.tresholdBrightness = tresholdBrightness;
  }
  public PImage transform(PImage inImg, PApplet app) {
    return myFilter(inImg, mode, tresholdBrightness);
  }
}

//different modes: tresholdBinary or tresholdBinaryInverted
PImage myFilter(PImage in, String mode, float tresholdBrightness) {

  PImage result = createImage(in.width, in.height, ALPHA);
  boolean biggerThanTreshold = false;
  for (int i = 0; i < in.width * in.height; i++) {
    biggerThanTreshold = 
      brightness(in.pixels[i]) > tresholdBrightness;
    if (biggerThanTreshold && mode.equals("tresholdBinary"))
      result.pixels[i] = in.pixels[i];
    else if (!biggerThanTreshold && mode.equals("tresholdBinary"))
      result.pixels[i] = 0;
    else if (biggerThanTreshold && mode.equals("tresholdBinaryInverted"))
      result.pixels[i] = 0;
    else if (!biggerThanTreshold && mode.equals("tresholdBinaryInverted"))
      result.pixels[i] = in.pixels[i];
  }
  return result;
}