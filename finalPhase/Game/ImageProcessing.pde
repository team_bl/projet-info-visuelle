import java.util.List; //<>// //<>// //<>//
import java.util.ArrayList;
import java.util.Collections;
import processing.video.*;

float[] tabSin = null;
float[] tabCos = null;
HScrollbar tresholdBar, scroll1, scroll2;

class ImageProcessingApplet extends PApplet {
  Capture cam;
  Button hueButton;
  QuadGraph quadGraph;
  float tresholdBrightness;

  boolean useWebcam = false;
  String filepath = "/home/simonlbc/workspace/Introvisu/projet-info-visuelle/finalPhase/Game/board4.jpg";
  TwoDThreeD twoDTo3D = null;
  PImage img;

  Movie mv;
  void movieSetup() {
    mv = new Movie(this, "/home/simonlbc/workspace/Introvisu/projet-info-visuelle/finalPhase/Game/testvideo.mp4"); //Put the video in the same directory
    mv.loop();
  }
  
  public void setup() {
    if (useWebcam) {
      webcamSetup();
      img = cam.get();
    } else {
      movieSetup();
      img = loadImage(filepath);
    }
    final float scrollHeight = img.height / 15.0;
    tresholdBar = new HScrollbar( 0, img.height - scrollHeight, img.width, scrollHeight);
    scroll1 = new HScrollbar(0, img.height - 2.5 * scrollHeight, img.width, scrollHeight);
    scroll2 = new HScrollbar(0, img.height - scrollHeight, img.width, scrollHeight);
    scroll1.sliderPosition = scroll1.newSliderPosition = 
      (0.53 * (scroll1.barWidth - scroll1.barHeight) + scroll1.xPosition); //empirical values of what the treshold value should be for the green board
    scroll2.sliderPosition = scroll2.newSliderPosition = 
      (0.41 * (scroll2.barWidth - scroll2.barHeight) + scroll2.xPosition); //empirical values of what the treshold value should be for the green board
    tresholdBrightness = brightness(color(128, 128, 128));
    hueButton = new Button(img.width * 3 / 4.0, img.height * 1 / 4.0, img.width / 15, img.width / 15);

    quadGraph = new QuadGraph();
    twoDTo3D = new TwoDThreeD(img.width, img.height);
  }

  public void settings() {
    size(1500, 800);
  }


  void webcamSetup() {
    String[] cameras = Capture.list();
    if (cameras.length == 0) {
      println("There are no cameras available for capture.");
      exit();
    } else {
      println("Available cameras:");
      for (int i = 0; i < cameras.length; i++) {
        println(cameras[i]);
      }
      cam = new Capture(this, cameras[0]);
      cam.start();
    }
  }

  class IdentityTrans implements Transformer {
    public PImage transform(PImage inImg, PApplet app) {
      return inImg;
    }
  }

  Transformer[] houghTransfs = new Transformer[] {
    new SobelTransformer()
  };
  Pipeline houghPipeline = new Pipeline(houghTransfs);

  //the identity pipe
  Pipeline idPipe = new Pipeline(new Transformer[] { new IdentityTrans() });

  PImage webcamImg;

  PVector getRotation() {
    return lastRotationInfo;
  }


  void movieEvent(Movie m) {
    m.read();
  }
  
  PVector lastRotationInfo = null;
  public void draw() {
    //image(img, 0, 0);
    //part1BeforeHue();
    //part1Hue();
    //part2Step1();
    //gaussianBlur(img);
    //image(houghPipeline.transform(img), 0, 0);
    //image(sobel(img), 0, 0);
    //image(hough(sobel(img))._2, 0, 0);
    //hueAndSobel(img);

    //drawHoughLines(houghPipeline, img);
    //                  change below boolean to true if you want to play with scroll bars
    //hueAndSobel(img);

    //======below processing detects lines of the given image=========
    if (useWebcam) {
      if (cam.available() == true) 
        cam.read();
      img = cam.get();
    } else {
      mv.read();
      img = mv.copy();
    }
    img.loadPixels();
    List<PVector> inters = lineDetect(img);
    lastRotationInfo = twoDTo3D.get3DRotations(inters);
    /*List<PVector> quad = quadGraph.build(inters, img.width, img.height);    
     if (quad != null) {
     quad = twoDTo3D.sortCorners(quad);
     println("QUAD VALUE of size " + quad.size() + ": ");
     for (PVector p: quad) {
     print(p.x + ", " + p.y + ", " + p.z);
     println();
     }
     println("done");
     for (PVector v : quad) {
     v.z = 1.f; // add 1 to vector
     }
     lastRotationInfo = twoDTo3D.get3DRotations(quad);
     } else println("WTF");
     */
    // image(lineDetectPipe.transform(img), 0, 0); //if you want to see what everything before gives as an output try this
  }


  final float[][] gaussianKernel = 
    {
    {9, 12, 9}, 
    {12, 15, 12}, 
    {9, 12, 9}
  };

  final float [][] bigGaussianKernel = 
    {
    {0.005374, 0.006088, 0.006708, 0.00719, 0.007495, 0.007599, 0.007495, 0.00719, 0.006708, 0.006088, 0.005374}, 
    {0.006088, 0.006897, 0.007599, 0.008145, 0.00849, 0.008609, 0.00849, 0.008145, 0.007599, 0.006897, 0.006088}, 
    {0.006708, 0.007599, 0.008374, 0.008974, 0.009355, 0.009486, 0.009355, 0.008974, 0.008374, 0.007599, 0.006708}, 
    {0.00719, 0.008145, 0.008974, 0.009618, 0.010026, 0.010166, 0.010026, 0.009618, 0.008974, 0.008145, 0.00719}, 
    {0.007495, 0.00849, 0.009355, 0.010026, 0.010452, 0.010598, 0.010452, 0.010026, 0.009355, 0.00849, 0.007495}, 
    {0.007599, 0.008609, 0.009486, 0.010166, 0.010598, 0.010746, 0.010598, 0.010166, 0.009486, 0.008609, 0.007599}, 
    {0.007495, 0.00849, 0.009355, 0.010026, 0.010452, 0.010598, 0.010452, 0.010026, 0.009355, 0.00849, 0.007495}, 
    {0.00719, 0.008145, 0.008974, 0.009618, 0.010026, 0.010166, 0.010026, 0.009618, 0.008974, 0.008145, 0.00719}, 
    {0.006708, 0.007599, 0.008374, 0.008974, 0.009355, 0.009486, 0.009355, 0.008974, 0.008374, 0.007599, 0.006708}, 
    {0.006088, 0.006897, 0.007599, 0.008145, 0.00849, 0.008609, 0.00849, 0.008145, 0.007599, 0.006897, 0.006088}, 
    {0.005374, 0.006088, 0.006708, 0.00719, 0.007495, 0.007599, 0.007495, 0.00719, 0.006708, 0.006088, 0.005374}, 
  };



  Pipeline lineDetectPipe = new Pipeline(
    new Transformer[] {
    new HueTransformer(), 
    new SaturationTransformer(), 
    new GaussianTransformer(gaussianKernel), 
    new TresholdTransformer("tresholdBinary", 120), 
    new SobelTransformer()
    }
    );

  List<PVector> lineDetect(PImage img) {

    //drawHoughLines(lineDetectPipe, img);
    //image(lineDetectPipe.transform(img), 0, 0);
    Hough h = new Hough(this);
    int bestLines = 10;
    PImage preTrsfo =lineDetectPipe.transform(img, this);
    List<PVector> inters = h.getIntersections(h.hough(preTrsfo, img, bestLines));

    return inters;
  }


  Pipeline huePipe = new Pipeline(
    new Transformer[] {
    new SobelTransformer()
    }
    );
  String currentStep = "hue"; 
  PImage curImg;
  void hueAndSobel(PImage img) {

    switch(currentStep) {
    case "hue":
      curImg = part1Hue(img, this, true);
      hueButton.update();
      hueButton.display();
      currentStep = hueButton.wasPressed ? "transform" : "hue";
      break;

    case "transform":
      curImg = huePipe.transform(curImg, this);
      currentStep = "display";

    case "display":
      image(curImg, 0, 0);
      break;

    default:
      throw new IllegalStateException();
    }
  }

  void part1BeforeHue() {
    image(myFilter(img, "tresholdBinaryInverted", 120), 0, 0);
    tresholdBar.display();
    tresholdBar.update();
    tresholdBrightness = 255 * tresholdBar.getPos();
  }




  final float[][] kernel1 = 
    { 
    { 0, 0, 0 }, 
    { 0, 2, 0 }, 
    { 0, 0, 0 } 
  };
  final float[][] kernel2 =
    {
    {0, 1, 0}, 
    {1, 0, 1}, 
    {0, 1, 0}
  };

  void part2Step1() {
    //choose kernel1 or kernel2 
    image(convolute(img, kernel1), 0, 0);
  }
}

class Pair<T1, T2> {
  public T1 _1;
  public T2 _2;
  public Pair(T1 _1, T2 _2) {
    this._1 = _1;
    this._2 = _2;
  }
}

//you have to input some kernels that are symmetric in size, and of which size is odd.
PImage convolute(PImage img, float[][] kernel) {
  float weight = 0;
  for (int i = 0; i < kernel.length; i++) 
    for (int j = 0; j < kernel[0].length; j++) 
      weight += kernel[i][j];  
  if (weight == 0)
    weight = 1.f;
  // create a greyscale image (type: ALPHA) for output
  PImage result = createImage(img.width, img.height, ALPHA);
  // kernel size N = 3
  int N = kernel.length;
  float sum = 0, brightness;
  for (int x = 1; x < img.width - 1; x++) {
    for (int y = 1; y < img.height - 1; y++) {
      sum = 0;
      for (int xPrim = max(x - N / 2, 0); xPrim <= x + N / 2 && xPrim < img.width; xPrim++) {
        for (int yPrim = max(y - N / 2, 0); yPrim <= y + N / 2 && yPrim < img.height; yPrim++) {
          brightness = brightness(img.pixels[yPrim * img.width + xPrim]);
          sum += brightness * kernel[N / 2 + (yPrim - y)][N / 2 + (xPrim - x)];
        }
      }
      result.pixels[y * img.width + x] = color(sum / weight);
    }
  }
  return result;
}