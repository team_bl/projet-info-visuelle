class SaturationTransformer implements Transformer {
  public PImage transform(PImage inImg, PApplet app) {
    return applySaturationThreshold(inImg);
  }

  PImage applySaturationThreshold(PImage imgRaw) {
    //VALUE CHOSEN
    float threshold = 90;
    PImage img = createImage(imgRaw.width, imgRaw.height, RGB);
    for (int i = 0; i < img.width * img.height; i++) {
      if (saturation(imgRaw.pixels[i]) <= threshold) {
        img.pixels[i] = color(0); //white
      } else {
        img.pixels[i] = color(255); //black
      }
    }
    return img;
  }
}