float depth = 1000;
float boardSpeed = 1.0;
float xRotation = 0.0;
float zRotation = 0.0;

void settings() {
  size(600, 600, P3D);
}
void setup() {
  noStroke();
}

void draw() {
  background(200);
  camera(width/2, height/2, depth, 250, 250, 0, 0, 1, 0);
  directionalLight(50, 100, 125, 0, -1, 0);
  ambientLight(102, 102, 102);
  translate(width/2, height/2, 0);
  
  rotateX(xRotation);
  rotateZ(zRotation);
  box(500, 30, 500);
}

void mouseDragged() {
  if (mouseY != pmouseY) {
    xRotation += map(pmouseY - mouseY, 0.0, height/4.0, 0.0, PI/3.0)*boardSpeed;
    if (xRotation > PI/3.0) {
      xRotation = PI/3.0;
    } else if (xRotation < -PI/3.0) {
      xRotation = -PI/3.0;
    }
}
  if (mouseX != pmouseX) {
    zRotation += map(mouseX - pmouseX, 0.0, width/4.0, 0.0, PI/3.0)*boardSpeed;
    if (zRotation > PI/3.0) {
      zRotation = PI/3.0;
    } else if (zRotation < -PI/3.0) {
      zRotation = -PI/3.0;
    }
  }
}
void mouseWheel(MouseEvent event) {
  boardSpeed += event.getCount()/20.0;
  if (boardSpeed > 1.5){
    boardSpeed = 1.5;
  } else if (boardSpeed < 0.2) {
    boardSpeed = 0.2;
  }
  println(boardSpeed);
}