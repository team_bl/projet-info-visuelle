import processing.video.*;
import java.util.Collections;
import java.util.List;

int threshold = 128;
int hueMin = 128;
int hueMax = 128;

PImage img;
Capture cam;

HScrollbar thresholdBar;
HScrollbar hueBarMax;
HScrollbar hueBarMin;

float[] tabSin = null;
float[] tabCos = null;

void settings() {
  size(800,600);
}

void setup() {
 /* String[] cameras = Capture.list();
  if (cameras.length == 0) {
    println("There are no cameras available for capture.");
    exit();
  } else {
    println("Available cameras:");
    for (int i = 0; i < cameras.length; i++) {
      println(cameras[i]);
    }
    cam = new Capture(this, cameras[0]);
    cam.start();
  }*/
  img = loadImage("../../board2.jpg");
  thresholdBar = new HScrollbar(0, height - 80, 800, 20);
  hueBarMax = new HScrollbar(0, height - 20, 800, 20);
  hueBarMin = new HScrollbar(0, height - 50, 800, 20);
}

void draw() {
  /*background(color(0,0,0));
  if (cam.available()) {
    cam.read();
  }
  img = cam.get();*/
  //image(img, 0, 0);
  //PImage img2 = sobel(gaussianBlur(applyThresholdBinary(applyHue(img))));
  //image(img2, img.width, 0);
  
  PImage img1 = sobel(img);
  image(img, 0, 0);
  getIntersections(hough(img1,10));

/*
  hueBarMax.display();
  hueBarMax.update();
  hueBarMin.display();
  hueBarMin.update();
  thresholdBar.display();
  thresholdBar.update();
  */
}

//Thresholding, color white if threshold reached, black ortherwise
PImage applyThresholdBinary(PImage imgRaw) {
  threshold = (int) (thresholdBar.getPos() * 255);
  PImage img = createImage(imgRaw.width, imgRaw.height, RGB);
  for(int i = 0; i < img.width * img.height; i++) {
    if (brightness(imgRaw.pixels[i]) <= threshold) {
      img.pixels[i] = color(0);
    } else {
      img.pixels[i] = color(255);
    }
  }
  return img;
}

//Thresholding, color black if threshold reached, white ortherwise
PImage applyThresholdBinaryInverted(PImage imgRaw) {
  threshold = (int) (thresholdBar.getPos() * 255);
  PImage img = createImage(imgRaw.width, imgRaw.height, RGB);
  for(int i = 0; i < img.width * img.height; i++) {
    if (brightness(imgRaw.pixels[i]) <= threshold) {
      img.pixels[i] = color(255);
    } else {
      img.pixels[i] = color(0);
    }
  }
  return img;
}

//hue algorithm, pixel selection from color range
PImage applyHue(PImage imgRaw) {
  hueMax = (int) (hueBarMax.getPos() * 255);
  hueMin = (int) (hueBarMin.getPos() * 255);
  PImage img = createImage(imgRaw.width, imgRaw.height, RGB);
  for(int i = 0; i < img.width * img.height; i++) {
    int hue = (int) hue(imgRaw.pixels[i]);
    if (hue >= hueMin && hue <= hueMax) {
      img.pixels[i] = imgRaw.pixels[i];
    } else {
      img.pixels[i] = color(0);
    }
  }
  return img;
}

//Returns weight of a matrix (1 if 0;)
float returnWeights(float[][] kernel) {
  float weight = 0.f;
  for (int i = 0; i< kernel.length; i++) {
    for (int j = 0; j < kernel[0].length; j++) {
      weight += kernel[i][j];
    }
  }
  return (weight == 0.f) ? 1 : weight;
}

/**
* convolute image applying the kernel
**/
PImage convolute(PImage img, float[][] kernel) {
  if (kernel == null){
    kernel =new float[][] {{0, 0, 0}, {0, 2, 0}, {0, 0, 0}};
  }
  PImage result = createImage(img.width, img.height, ALPHA);
  float weight = returnWeights(kernel);
  
  for (int i = img.width + 1; i < img.width * (img.height - 1) - 1; i++) {
    if ((i % img.width != 0) && (i % img.width != width - 1)){
      float newValue = 0.f;
      for (int ligne = 0; ligne < kernel.length; ligne ++) {
        for (int colonne = 0; colonne < kernel[0].length; colonne++) {
          newValue += brightness(img.pixels[i + (ligne - 1)*img.width + (colonne - 1)]) * kernel[ligne][colonne];      
        }
      }
      result.pixels[i] = color((int)(newValue/weight));
    }
  }
  return result;
}

//specific convolution : blur
PImage gaussianBlur(PImage img) {
  float[][] kernel = {{9, 12, 9},
                      {12, 15, 12}, 
                      {9, 12, 9}};
  return convolute(img, kernel);
}

//specific convolution (double convolution)
PImage sobel(PImage img) {
  float[][] hKernel = {{0, 1, 0},
                       {0, 0, 0},
                       {0, -1, 0}};
  
  float[][] vKernel = {{0, 0, 0},
                       {1, 0, -1},
                       {0, 0, 0}};
  PImage result = createImage(img.width, img.height, ALPHA);
  // clear the image
  for (int i = 0; i < img.width * img.height; i++) {
    result.pixels[i] = color(0);
  }
  float max=0;
  float[] buffer = new float[img.width * img.height];
  
  // double convolution
  for (int y = 2; y < img.height - 2; y++) {
  // Skip top and bottom edges
    for (int x = 2; x < img.width - 2; x++) {
    // Skip left and right
      float sum_h = 0.f;
      float sum_v = 0.f;

      for (int ligne = 0; ligne < hKernel.length; ligne ++) {
        for (int colonne = 0; colonne < hKernel[0].length; colonne++) {
          sum_h += brightness(img.pixels[(y * img.width + x) + (ligne - 1)*img.width + (colonne - 1)]) * hKernel[ligne][colonne];
          sum_v += brightness(img.pixels[(y * img.width + x) + (ligne - 1)*img.width + (colonne - 1)]) * vKernel[ligne][colonne];
        }
      }
      //save sum into buffer
      float sum = sqrt(sum_h * sum_h + sum_v * sum_v);
      buffer[y * img.width + x] = sum;
      //update max
      max = (sum > max) ? sum : max;
    }
  }
  //iterate over the buffer and set pixels over 30% of max
  for (int i = 0; i < buffer.length; i++){
    if (buffer[i] > (int)(max * 0.3f)) {
    // 30% of the max
      result.pixels[i] = color(255);
    } else {
      result.pixels[i] = color(0);
    }
  }
  return result;
}