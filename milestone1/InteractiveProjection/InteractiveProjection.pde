/****************************
*          Main             *
****************************/
float angleX = 0.0;
float angleY = 0.0;
float scale = 1.0;

void settings() {
  size(600, 600, P2D);
}

void setup() {
}

void draw() {
  background(255, 255, 255);
  My3DPoint eye = new My3DPoint(0, 0, -5000);
  My3DPoint origin = new My3DPoint(0, 0, 0);
  My3DBox input3DBox = new My3DBox(origin, 100, 150, 300);
  float[][] transform0 = translationMatrix(-50, -75, -150);
  input3DBox = transformBox(input3DBox, transform0);
  
  //scale
  float[][] transform1 = scaleMatrix(scale, scale, scale);
  input3DBox = transformBox(input3DBox, transform1);

  //rotated around x
  float[][] transform2 = rotateXMatrix(angleX);
  input3DBox = transformBox(input3DBox, transform2);
  
  //rotated around y
  float[][] transform3 = rotateYMatrix(angleY);
  input3DBox = transformBox(input3DBox, transform3);
  
  //rotated center
  float[][] center = translationMatrix(300, 300, 300);
  input3DBox = transformBox(input3DBox, center);
  
  projectBox(eye, input3DBox).render();

}

void mouseDragged() {
  if (mouseY > pmouseY) {
    scale += (mouseY - pmouseY)/15.0;
  } else if (mouseY < pmouseY) {
    scale -= (pmouseY - mouseY)/15.0;
  }
}

void keyPressed() {
  if (key == CODED) {
	  switch (keyCode) {
		  case UP :
			angleX += PI/20;
			break;
		  case DOWN :
			angleX -= PI/20;
			break;
		  case RIGHT :
			angleY += PI/20;
                  break;
		  case LEFT :
			angleY -= PI/20;
                  break;
		  default :
	  }
  }
}

My2DPoint projectPoint(My3DPoint eye, My3DPoint p) {
  // matrix calculation gives this result - not explicited here
  return new My2DPoint(eye.z * (p.x - eye.x) / (eye.z - p.z),
  eye.z * (p.y - eye.y) / (eye.z - p.z));
}

My2DBox projectBox(My3DPoint eye, My3DBox box) {
  My2DPoint[] ls = new My2DPoint[8];
  for (int i=0; i<8; i++) {
    ls[i] = projectPoint(eye, box.p[i]);
  }
  return new My2DBox(ls);
}

My3DBox transformBox(My3DBox box, float[][] transformMatrix) {
  My3DPoint[] p = new My3DPoint[8];
  for (int i=0; i<8; i++) {
    p[i] = euclidian3DPoint(matrixProduct(transformMatrix, homogeneous3DPoint(box.p[i])));
  }
  return new My3DBox(p);
}

/****************************
*		Box 8 points		*
****************************/
class My2DBox {
  My2DPoint[] s;
  My2DBox(My2DPoint[] s) {
    this.s = s;
  }
  
  void drawLine(My2DPoint a, My2DPoint b) {
    line (a.x, a.y, b.x, b.y);
  }
  
  void render() {
    colorMode(RGB);
    stroke(255, 0, 0);
    drawLine(s[0], s[1]);
    drawLine(s[1], s[2]);
    drawLine(s[2], s[3]);
    drawLine(s[3], s[0]);
    stroke(0, 255, 0);
    drawLine(s[4], s[5]);
    drawLine(s[5], s[6]);
    drawLine(s[6], s[7]);
    drawLine(s[7], s[4]);
    stroke(0, 0, 255);
    drawLine(s[0], s[4]);
    drawLine(s[1], s[5]);
    drawLine(s[2], s[6]);
    drawLine(s[3], s[7]);
  }
}

class My3DBox {
  My3DPoint[] p;
  My3DBox(My3DPoint origin, float dimX, float dimY, float dimZ) {
    float x = origin.x;
    float y = origin.y;
    float z = origin.z;
    this.p = new My3DPoint[]{
      new My3DPoint(x, y+dimY, z+dimZ),
      new My3DPoint(x, y, z+dimZ),
      new My3DPoint(x+dimX, y, z+dimZ),
      new My3DPoint(x+dimX, y+dimY, z+dimZ),
      new My3DPoint(x, y+dimY, z),
      origin,
      new My3DPoint(x+dimX, y, z),
      new My3DPoint(x+dimX, y+dimY, z)
    };
  }
  My3DBox(My3DPoint[] p) {
    this.p = p;
  }
}

/****************************
*		Matrices 4x4		*
****************************/
// All the matrix given here are 4x4

float[][] rotateXMatrix(float angle) {
  return (new float[][] {
    {1, 0, 0, 0},
    {0, cos(angle), sin(angle), 0},
    {0, -sin(angle), cos(angle), 0},
    {0, 0, 0, 1}
  });
}

float[][] rotateYMatrix(float angle) {
  return (new float[][] {
    {cos(angle), 0, sin(angle), 0},
    {0, 1, 0, 0},
    {-sin(angle), 0, cos(angle), 0},
    {0, 0, 0, 1}
  });
}

float[][] rotateZMatrix(float angle) {
  return (new float[][] {
    {cos(angle), sin(angle), 0, 0},
    {-sin(angle), cos(angle), 0, 0},
    {0, 0, 1, 0},
    {0, 0, 0, 1}
  });
}

float[][] scaleMatrix(float x, float y, float z) {
  return (new float[][] {
    {x, 0, 0, 0},
    {0, y, 0, 0},
    {0, 0, z, 0},
    {0, 0, 0, 1}
  });
}

float[][] translationMatrix(float x, float y, float z) {
  return (new float[][] {
    {1, 0, 0, x},
    {0, 1, 0, y},
    {0, 0, 1, z},
    {0, 0, 0, 1}
  });
}

float[] matrixProduct(float[][] a, float[] b) {
  float[] result = new float[4];
  for (int i=0; i<4; i++) {
    for (int j=0; j<4; j++) {
      result[i] += a[i][j] * b[j];
    }
  }
  return result;
}

/****************************
*			Points			*
****************************/
class My2DPoint {
  float x;
  float y;
  My2DPoint(float x, float y) {
    this.x = x;
    this.y = y;
  }
}

class My3DPoint {
  float x, y, z;
  My3DPoint(float x, float y, float z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }
}

float[] homogeneous3DPoint(My3DPoint p) {
  return new float[]{p.x, p.y, p.z, 1};
}

My3DPoint euclidian3DPoint(float[] a) {
  return new My3DPoint(a[0]/a[3], a[1]/a[3], a[2]/a[3]);
}