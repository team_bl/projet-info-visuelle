float boardSpeed = 1.0f;

float xRotation = 0.0f;
float zRotation = 0.0f;

float boxHeight = 10.0f;
float boxEdges = 500.0f;

float distanceDataToViewBox = boxEdges/2.0f * 1.6;

final float GRAVITY_CST = 0.5f;

boolean isShiftPressed = false;

final ArrayList<PVector> cylindersPos = new ArrayList<PVector>();
HScrollbar scrollBar;

Mover mover;

double score = 0.0;
double lastScore = 0.0;

void settings() {
  size(1000, 600, P3D);
}
void setup() {
  noStroke();
  mover = new Mover();
  initCylinder(); 
  dataBackground = createGraphics(1400, (int) DATA_HEIGHT, P2D);
  topView = createGraphics((int)TOP_VIEW_EDGES, (int)TOP_VIEW_EDGES, P2D);
  scoreboard = createGraphics((int)TOP_VIEW_EDGES, (int)TOP_VIEW_EDGES, P2D);
  barChart = createGraphics((int)(width - 4 * TOP_VIEW_MARGIN - 2 * TOP_VIEW_EDGES),(int) SCORE_BOARD_HEIGHT, P2D);
  scrollBar = new HScrollbar(3 * TOP_VIEW_MARGIN + 2 *TOP_VIEW_EDGES, height - SCROLL_HEIGHT - TOP_VIEW_MARGIN, (width - 4 * TOP_VIEW_MARGIN - 2 * TOP_VIEW_EDGES)/2, SCROLL_HEIGHT);
}

void draw() {
  background(250);
  directionalLight(50, 100, 125, 0, -1, 0);
  ambientLight(102, 102, 102);
  translate(width/2, height/2, -distanceDataToViewBox);
  if (isShiftPressed){
    pushMatrix();
    translate(0, 0, distanceDataToViewBox);
    rotateX(-PI/2);
    fill(200);
    box(boxEdges, boxHeight, boxEdges);
    fill(255);
    mover.display();
    displayCylinders();
    popMatrix();
  } else {
    pushMatrix();
    rotateX(xRotation);
    rotateZ(zRotation);
    noFill();
    stroke(0);
    box(boxEdges, boxHeight, boxEdges);
    noStroke();
    fill(255);
    mover.update();
    mover.checkEdges();
    mover.checkCylinderCollision();
    mover.display();
    displayCylinders();
    popMatrix();
    pushMatrix();
    translate(-width/2, -height/2, distanceDataToViewBox);
    drawData();
    image(dataBackground, 0, height - DATA_HEIGHT);
    image(topView, TOP_VIEW_MARGIN,  height - DATA_HEIGHT + TOP_VIEW_MARGIN, TOP_VIEW_EDGES, TOP_VIEW_EDGES);
    image(scoreboard, 2 * TOP_VIEW_MARGIN + TOP_VIEW_EDGES, height - DATA_HEIGHT + TOP_VIEW_MARGIN, TOP_VIEW_EDGES, TOP_VIEW_EDGES);
    image(barChart, 3 * TOP_VIEW_MARGIN + 2 *TOP_VIEW_EDGES, height - DATA_HEIGHT + TOP_VIEW_MARGIN);
    scrollBar.update();
    scrollBar.display();
    noLoop();
    updateRectSize();
    loop();
    popMatrix();
  }
}

void mouseDragged() {
  if (mouseY < height - DATA_HEIGHT){ // no effect in dashboard
    if (mouseY != pmouseY) {
      xRotation += map(pmouseY - mouseY, 0.0, height/4.0, 0.0, PI/3.0)*boardSpeed;
      if (xRotation > PI/3.0) {
        xRotation = PI/3.0;
      } else if (xRotation < -PI/3.0) {
        xRotation = -PI/3.0;
      }
   }
    if (mouseX != pmouseX) {
      zRotation += map(mouseX - pmouseX, 0.0, width/4.0, 0.0, PI/3.0)*boardSpeed;
      if (zRotation > PI/3.0) {
        zRotation = PI/3.0;
      } else if (zRotation < -PI/3.0) {
        zRotation = -PI/3.0;
      }
    }
  }
}
void mouseWheel(MouseEvent event) {
  boardSpeed += event.getCount()/20.0;
  if (boardSpeed > 1.5){
    boardSpeed = 1.5;
  } else if (boardSpeed < 0.2) {
    boardSpeed = 0.2;
  }
  println(boardSpeed);
}

void keyPressed() {
  if (key == CODED) {
    if (keyCode == SHIFT) {
      println("SHFIT PRESSED");
      isShiftPressed = true;
      draw();
    }
  }
}

void keyReleased() {
  if (key == CODED) {
    if (keyCode == SHIFT) {
      println("SHFIT RELEASED");
      isShiftPressed = false;
    }
  }
}

void mouseClicked() {
  if (isShiftPressed) {
    if(mouseX < width/2 + boxEdges/2 && mouseX > width/2 - boxEdges/2 
         && mouseY < height/2 + boxEdges/2 && mouseY > height/2 - boxEdges/2) { 
      cylindersPos.add(new PVector(-(width/2 - mouseX), -(height/2 - mouseY)));
    }
  }
}