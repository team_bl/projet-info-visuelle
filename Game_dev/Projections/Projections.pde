void settings() {
  size(1000, 1000, P2D);
}

void setup() {
  background(255, 255, 255);
}

void draw() {
  My3DPoint eye = new My3DPoint(0, 0, -5000);
  My3DPoint origin = new My3DPoint(0, 0, 0);
  My3DBox input3DBox = new My3DBox(origin, 100, 150, 300);
  
  //rotated around x
  float[][] transform1 = rotateXMatrix(PI/8);
  input3DBox = transformBox(input3DBox, transform1);
  projectBox(eye, input3DBox).render();
  
  //then translating
  float[][] transform2 = translationMatrix(200, 200, 0);
  input3DBox = transformBox(input3DBox, transform2);
  projectBox(eye, input3DBox).render();
  
  //finally scaling
  float[][] transform3 = scaleMatrix(2, 2, 2);
  input3DBox = transformBox(input3DBox, transform3);
  projectBox(eye, input3DBox).render();
}

My2DPoint projectPoint(My3DPoint eye, My3DPoint p) {
  // matrix calculation gives this result - not explicited here
  return new My2DPoint(eye.z * (p.x - eye.x) / (eye.z - p.z),
  eye.z * (p.y - eye.y) / (eye.z - p.z));
}

My2DBox projectBox(My3DPoint eye, My3DBox box) {
  My2DPoint[] ls = new My2DPoint[8];
  for (int i=0; i<8; i++) {
    ls[i] = projectPoint(eye, box.p[i]);
  }
  return new My2DBox(ls);
}

My3DBox transformBox(My3DBox box, float[][] transformMatrix) {
  My3DPoint[] p = new My3DPoint[8];
  for (int i=0; i<8; i++) {
    p[i] = euclidian3DPoint(matrixProduct(transformMatrix, homogeneous3DPoint(box.p[i])));
  }
  return new My3DBox(p);
}