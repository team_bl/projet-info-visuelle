float gravityConstant = 1;
float bouncingConstant = 1;

class Mover {
  PVector location;
  PVector velocity;
  PVector gravity;
  int boxX = 200;
  int boxZ = 200;
  
  Mover() {
    location = new PVector(0, -20, 0);
    velocity = new PVector(0, 0, 0);
    gravity = new PVector(0, 0, 0);
  }
  
  void update(float rx, float rz) {
    gravity.x = sin(rz) * gravityConstant;
    gravity.z = -sin(rx) * gravityConstant;
  
    float normalForce = 1;
    float mu = 0.01;
    float frictionMagnitude = normalForce * mu;
    PVector friction = velocity.get();
    friction.mult(-1);
    friction.normalize();
    friction.mult(frictionMagnitude);
    
    velocity.add(friction);
    
    velocity.add(gravity);
    location.add(velocity);
  }
  
  void display() {
    stroke(0);
    strokeWeight(2);
    fill(127);
    pushMatrix();
    translate(location.x, location.y, location.z);
    sphere(20);
    popMatrix();
  }
  
  void checkEdges() {
    if(location.x>boxX/2) {
      location.x = boxX/2;
      velocity.x *= -bouncingConstant;
    } else if (location.x < -boxX/2) {
      location.x = -boxX/2;
      velocity.x *= -bouncingConstant;
    }
    
    if(location.z > boxZ/2) {
      location.z = boxZ/2;
      velocity.z *= -bouncingConstant;
    } else if (location.z < -boxZ/2) {
      location.z = -boxZ/2;
      velocity.z *= -bouncingConstant;
    }
  }
}