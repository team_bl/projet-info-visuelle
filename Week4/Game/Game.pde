int speed = 0; // between -8 and +5
float rotationX = 0; //between -60 and 60
float rotationZ = 0; // idem above

Mover mover;

void settings() {
  size(500, 500, P3D);
}

void setup() {
  noStroke();
  mover = new Mover();
}

void draw() {
  camera(width/2, height/2, -700, 250, 250, 0, 0, 1, 0);
  directionalLight(50, 100, 125, 0, 1, 0);
  ambientLight(102, 102, 102);
  background(255);
  translate(width/2, height/2, 0);
  float rx = map(rotationX, -60, 60, -PI/3, PI/3);
  float rz = map(rotationZ, -60, 60, -PI/3, PI/3);
  rotateZ(rz);
  rotateX(rx);
  box(200, 10, 200);
  
  mover.update(rx, rz);
  mover.checkEdges();
  mover.display();
}

void mouseDragged() {
  rotationX += (pmouseY - mouseY)*(1.0+speed*0.1);
  if (rotationX > 60) {
    rotationX = 60;
  } else if (rotationX < -60) {
    rotationX = -60;
  }
  rotationZ += (pmouseX - mouseX)*(1.0+speed*0.1);
  if (rotationZ > 60) {
    rotationZ = 60;
  } else if (rotationZ < -60) {
    rotationZ = -60;
  }
}

void mouseWheel(MouseEvent event) {
  speed -= event.getCount();
  if(speed < -8){
    speed = -8;
  } else if (speed > 5) {
    speed = 5;
  }
}