PShape cylinder = new PShape();

float cylinderBaseSize = 15;
float cylinderHeight = 50;
int cylinderResolution = 40;
boolean initialized = false;

PShape initCylinder() {
  if(initialized) {
    return cylinder;
  }
  
  float angle;
  float[] x = new float[cylinderResolution + 1];
  float[] y = new float[cylinderResolution + 1];
  
  //get the x and y position on a circle for all the sides
  for (int i = 0; i < x.length; i++) {
    angle = (TWO_PI / cylinderResolution) * i;
    x[i] = sin(angle) * cylinderBaseSize;
    y[i] = cos(angle) * cylinderBaseSize;
  }
  
  cylinder = createShape();
  
  //draw the border of the cylinder
  cylinder.beginShape(QUAD_STRIP);
  int opt = 2;
  for (int i = 0; i < x.length; i++) {
    if (opt == 1) {
      cylinder.vertex(x[i], y[i], 0);
      cylinder.vertex(x[i], y[i], cylinderHeight);
    } else {
      cylinder.vertex( y[i], x[i], 0);
      cylinder.vertex(y[i], x[i], cylinderHeight);
    }
  }
  cylinder.endShape();
  
  //draw the top closure of the cylinder
  cylinder.beginShape(TRIANGLE_FAN);
  cylinder.vertex(0, 0, cylinderHeight);
  for (int i = 0; i < x.length; i++) {
    cylinder.vertex(x[i], y[i], cylinderHeight);
  }
  cylinder.endShape();
  
  //draw the bottom closure of the cylinder
  cylinder.beginShape(TRIANGLE_FAN);
  cylinder.vertex(0, 0, 0);
  for (int i = 0; i < x.length; i++) {
    cylinder.vertex(x[i], y[i], 0);
  }
  cylinder.endShape();
  
  initialized = true;
  return cylinder;
}

void displayCylinders() {
  for (PVector v : cylindersPos) {
    pushMatrix();
    translate(v.x, 0, v.y);
    rotateX(PI/2);
    stroke(0);
    shape(cylinder);
    noStroke();
    popMatrix();
  }
}