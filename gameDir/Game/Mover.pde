float gravityConstant = 1;
float bouncingConstant = 1;

class Mover {
  PVector location;
  PVector velocity;
  PVector gravity;
  
  Mover() {
    location = new PVector(0, -20, 0);
    velocity = new PVector(0, 0, 0);
    gravity = new PVector(0, 0, 0);
  }
  
  void update(float rx, float rz) {
    gravity.x = sin(rz) * gravityConstant;
    gravity.z = -sin(rx) * gravityConstant;
  
    float normalForce = 1;
    float mu = 0.01;
    float frictionMagnitude = normalForce * mu;
    PVector friction = velocity.get();
    friction.mult(-1);
    friction.normalize();
    friction.mult(frictionMagnitude);
    
    velocity.add(friction);
    
    velocity.add(gravity);
    location.add(velocity);
  }
  
  void display() {
    noStroke();
    fill(200);
    pushMatrix();
    translate(location.x, location.y, location.z);
    sphere(20);
    popMatrix();
  }
  
  void checkEdges() {
    if(location.x>boxSize/2) {
      location.x = boxSize/2;
      velocity.x *= -bouncingConstant;
    } else if (location.x < -boxSize/2) {
      location.x = -boxSize/2;
      velocity.x *= -bouncingConstant;
    }
    
    if(location.z > boxSize/2) {
      location.z = boxSize/2;
      velocity.z *= -bouncingConstant;
    } else if (location.z < -boxSize/2) {
      location.z = -boxSize/2;
      velocity.z *= -bouncingConstant;
    }
  }
  
  void checkCylinderCollision() {
    for (PVector v : cylindersPos) {
      if(distance(v) < 35) {
        PVector normal = new PVector(location.x, 0, location.z);
        normal.sub(v.x, 0, v.y);
        normal.normalize();
        PVector tempVelo = new PVector(velocity.x, 0, velocity.z);
        velocity = velocity.sub(normal.mult(2*(tempVelo.dot(normal))));
        velocity.mult(bouncingConstant);
      }
    }
  }
  
  float distance(PVector v) {
    return sqrt((v.x - location.x)*(v.x - location.x)
    + (v.y - location.z)*(v.y - location.z));
  }
}