/**
* Only 8 points : it is a box
*/
class My2DBox {
  My2DPoint[] s;
  My2DBox(My2DPoint[] s) {
    this.s = s;
  }
  
  void drawLine(My2DPoint a, My2DPoint b) {
    line (a.x, a.y, b.x, b.y);
  }
  
  void render() {
    colorMode(RGB);
    stroke(255, 0, 0);
    drawLine(s[0], s[1]);
    drawLine(s[1], s[2]);
    drawLine(s[2], s[3]);
    drawLine(s[3], s[0]);
    stroke(0, 255, 0);
    drawLine(s[4], s[5]);
    drawLine(s[5], s[6]);
    drawLine(s[6], s[7]);
    drawLine(s[7], s[4]);
    stroke(0, 0, 255);
    drawLine(s[0], s[4]);
    drawLine(s[1], s[5]);
    drawLine(s[2], s[6]);
    drawLine(s[3], s[7]);
  }
}

class My3DBox {
  My3DPoint[] p;
  My3DBox(My3DPoint origin, float dimX, float dimY, float dimZ) {
    float x = origin.x;
    float y = origin.y;
    float z = origin.z;
    this.p = new My3DPoint[]{
      new My3DPoint(x, y+dimY, z+dimZ),
      new My3DPoint(x, y, z+dimZ),
      new My3DPoint(x+dimX, y, z+dimZ),
      new My3DPoint(x+dimX, y+dimY, z+dimZ),
      new My3DPoint(x, y+dimY, z),
      origin,
      new My3DPoint(x+dimX, y, z),
      new My3DPoint(x+dimX, y+dimY, z)
    };
  }
  My3DBox(My3DPoint[] p) {
    this.p = p;
  }
}