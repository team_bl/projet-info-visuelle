float gravityConstant = 1;
float bouncingConstant = 0.90;
final int sphereRadius = 20;

class Mover {
  PVector location;
  PVector velocity;
  PVector gravity;
  
  Mover() {
    location = new PVector(0, -sphereRadius, 0);
    velocity = new PVector(0, 0, 0);
    gravity = new PVector(0, 0, 0);
  }
  
  void update(float rx, float rz) {
    gravity.x = sin(rz) * gravityConstant;
    gravity.z = -sin(rx) * gravityConstant;
  
    float normalForce = 1;
    float mu = 0.01;
    float frictionMagnitude = normalForce * mu;
    PVector friction = velocity.get();
    friction.mult(-1);
    friction.normalize();
    friction.mult(frictionMagnitude);
    
    velocity.add(friction);
    velocity.add(gravity);
    location.add(velocity);
  }
  
  void display() {
    noStroke();
    fill(200);
    pushMatrix();
    translate(location.x, location.y, location.z);
    sphere(sphereRadius);
    popMatrix();
  }
  
  void checkEdges() {
    int boxLimit = boxSize/2 - sphereRadius;
    if(location.x>boxLimit) {
      location.x = boxLimit;
      velocity.x *= -bouncingConstant;
    } else if (location.x < -boxLimit) {
      location.x = -boxLimit;
      velocity.x *= -bouncingConstant;
    }
    
    if(location.z > boxLimit) {
      location.z = boxLimit;
      velocity.z *= -bouncingConstant;
    } else if (location.z < -boxLimit) {
      location.z = -boxLimit;
      velocity.z *= -bouncingConstant;
    }
  }
  
  void checkCylinderCollision() {
    for (PVector v : cylindersPos) {
      PVector distV = distVect(v);
      float dist = distV.mag();
      float minDist = sphereRadius + cylinderBaseSize;
      if(dist <= (minDist)) {
        //la boule n est pas dans le cylindre quand elle rebondit
        distV.mult(minDist/dist);
        location.x = v.x - distV.x;
        location.z = v.y - distV.y;
        
        //rebond       
        PVector normal = new PVector(location.x, 0, location.z);
        normal.sub(v.x, 0, v.y);
        normal.normalize();
        PVector tempVelo = new PVector(velocity.x, 0, velocity.z);
        velocity = velocity.sub(normal.mult(2*(tempVelo.dot(normal))));
        velocity.mult(bouncingConstant);
      }
    }
  }
  
  PVector distVect(PVector v) {
    return new PVector((v.x - location.x), (v.y - location.z));
  }
}