PShape cylinder = new PShape();
PShape cylinderTop = new PShape();

float cylinderBaseSize = 15;
float cylinderHeight = 50;
int cylinderResolution = 40;

void initCylinder() {
  float angle;
  float[] x = new float[cylinderResolution + 1];
  float[] y = new float[cylinderResolution + 1];
  
  //get the x and y position on a circle for all the sides
  for (int i = 0; i < x.length; i++) {
    angle = (TWO_PI / cylinderResolution) * i;
    x[i] = sin(angle) * cylinderBaseSize;
    y[i] = cos(angle) * cylinderBaseSize;
  }
  
  cylinder = createShape();
  cylinder.beginShape(QUAD_STRIP);
  //draw the border of the cylinder
  for(int i = 0; i < x.length; i++) {
    cylinder.vertex(x[i], y[i], 0);
    cylinder.vertex(x[i], y[i], cylinderHeight);
  }
  cylinder.endShape();
  
  //draw the top closure of the cylinder
  cylinderTop = createShape();
  cylinderTop.beginShape(TRIANGLE_FAN);
  cylinderTop.vertex(0, 0, cylinderHeight);
  for (int i = 0; i < x.length; i++) {
    cylinderTop.vertex(x[i], y[i], cylinderHeight);
  }
  cylinderTop.endShape();
}

void displayCylinders() {
  for (PVector v : cylindersPos) {
    pushMatrix();
    rotateX(PI/2);
    translate(v.x, v.y, boxHeight/2.0);
    fill(255);
    shape(cylinder);
    shape(cylinderTop);
    popMatrix();
  }
}