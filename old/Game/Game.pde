/* ------------------------------
             Constants
   ------------------------------*/

int speed = 0; // between -8 and +5
float rotationX = 0; //between -60 and 60
float rotationZ = 0; // idem above
final int boxSize = 400;
final int box_thickness = 10;

final int SHIFT_CODE = 1; 
/* use power of two when you initialize other states,
 * e.g. if another key code was to be used add it
 * add it as final int KEY_CODE_EXAMPLE = <power of two not used yet> 
 */
int keysPressed = 0;

/* ------------------------------
          General methods
   ------------------------------*/

final ArrayList<PVector> cylindersPos = new ArrayList<PVector>();
Mover mover;

void settings() {
  size(1400, 1000, P3D);
}

void setup() {
  noStroke();
  mover = new Mover();
  initCylinder(); 
}
void draw() {
  if (wasPressed(SHIFT_CODE)) {
    background(255);
    translate(width/2, height/2, 0);
    rotateX(-PI/2);
    fill(150);
    box(boxSize, 10, boxSize);
    mover.display();
    displayCylinders();
  } 

  if (keysPressed == 0) {
    
    directionalLight(50, 100, 125, 0, 1, 0);
    ambientLight(102, 102, 102);
    background(255);
    translate(width/2, height/2, 0);
    float rx = map(rotationX, -60, 60, -PI/3, PI/3);
    float rz = map(rotationZ, -60, 60, -PI/3, PI/3);
    rotateZ(rz);
    rotateX(rx);
    box(boxSize, box_thickness, boxSize);

    mover.update(rx, rz);
    mover.checkEdges();
    mover.checkCylinderCollision();
    
    translate(0, -box_thickness/2.0, 0);
    mover.display();
    displayCylinders();
  }
}

/* ------------------------------
          Mouse control
   ------------------------------*/

void mouseDragged() {
  if (!wasPressed(SHIFT_CODE)) {
    rotationX += (pmouseY - mouseY)*(1.0+speed*0.1);
    if (rotationX > 60) {
      rotationX = 60;
    } else if (rotationX < -60) {
      rotationX = -60;
    }
    rotationZ += (pmouseX - mouseX)*(1.0+speed*0.1);
    if (rotationZ > 60) {
      rotationZ = 60;
    } else if (rotationZ < -60) {
      rotationZ = -60;
    }
  }
}

void mouseWheel(MouseEvent event) {
  if (!wasPressed(SHIFT_CODE)) {
    speed -= event.getCount();
    if (speed < -8) {
      speed = -8;
    } else if (speed > 5) {
      speed = 5;
    }
  }
}

void mouseClicked() {
  if (wasPressed(SHIFT_CODE)) {
    if(mouseX < width/2 + boxSize/2 && mouseX > width/2 - boxSize/2 
         && mouseY < height/2 + boxSize/2 && mouseY > height/2 - boxSize/2) { 
      cylindersPos.add(new PVector(-(width/2 - mouseX), -(height/2 - mouseY)));
    }
  }
}

/* ------------------------------
          Keys control
   ------------------------------*/

void keyPressed() {
  recordPress(keyToCode(keyCode));
}

void keyReleased() {
  recordUnpress(keyToCode(keyCode));
}

boolean wasPressed(int KEY_CODE) {
  return (keysPressed & KEY_CODE) > 0;
}

void recordPress(int KEY_CODE) {
  keysPressed |= KEY_CODE;
}

void recordUnpress(int KEY_CODE) {
  if (wasPressed(KEY_CODE))
    keysPressed ^= KEY_CODE;
}

int keyToCode(int keyRec) {
  int code = 0;
  switch(keyRec) {
  case SHIFT: 
    code = SHIFT_CODE;
    break;
  }
  return code;
}