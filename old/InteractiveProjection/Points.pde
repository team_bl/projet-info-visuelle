class My2DPoint {
  float x;
  float y;
  My2DPoint(float x, float y) {
    this.x = x;
    this.y = y;
  }
}

class My3DPoint {
  float x, y, z;
  My3DPoint(float x, float y, float z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }
}

float[] homogeneous3DPoint(My3DPoint p) {
  return new float[]{p.x, p.y, p.z, 1};
}

My3DPoint euclidian3DPoint(float[] a) {
  return new My3DPoint(a[0]/a[3], a[1]/a[3], a[2]/a[3]);
}