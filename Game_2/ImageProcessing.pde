import processing.video.*;
import java.util.Collections;

class ImageProcessing extends PApplet {
  PImage img;
  Capture cam;
  int threshold;
  int hueMin;
  int hueMax;
  PVector rotation = new PVector(0,0,0);
  
  //HScrollbar thresholdBar;
  //HScrollbar hueBarMax;
  //HScrollbar hueBarMin;
  
  float[] tabSin;
  float[] tabCos;
  
  ArrayList<PVector> lines = null;
  QuadGraph quadGraph;
  TwoDThreeD twoD3D;
  
  void settings() {
    size(800,600);
  }
  
  void setup() {
    String[] cameras = Capture.list();
    if (cameras.length == 0) {
      println("There are no cameras available for capture.");
      exit();
    } else {
      println("Available cameras:");
      for (int i = 0; i < cameras.length; i++) {
        println(cameras[i]);
      }
      cam = new Capture(this, cameras[0]);
      cam.start();
    }
    img = loadImage("../board4.jpg");
    quadGraph = new QuadGraph();
    twoD3D = new TwoDThreeD(img.width, img.height);
     
    //thresholdBar = new HScrollbar(0, height - 80, 800, 20);
    //hueBarMin = new HScrollbar(0, height - 50, 800, 20);
    //hueBarMax = new HScrollbar(0, height - 20, 800, 20);
  }
  
  void draw() {
    if (cam.available()) {
      cam.read();
    }
    //img = cam.get();
    background(color(0,0,0));
    
    //FILTERS PIEPLINE
    //PImage img1 = sobel(applyThresholdBinary(gaussianBlur(applyHue(img))));
    PImage img1 = sobel(applyThresholdBinary(gaussianBlur(applySaturationThreshold(applyHue(img)))));
    image(img1, 0, 0);
    lines = hough(img1, 6);
    List<PVector> quad = quadGraph.build(lines, img1.width, img.height);
    if (quad != null) {
     quadGraph.sortCorners(quad);
     for (PVector v : quad) {
       v.z = 1.f; // add 1 to vector
     }
     rotation = twoD3D.get3DRotations(quad);
    }
    
    //DEBUG PURPOSE
    //hueBarMax.display();
    //hueBarMax.update();
    //hueBarMin.display();
    //hueBarMin.update();
    //thresholdBar.display();
    //thresholdBar.update();
  }
  
  
  public PVector getRotationAngles() {
    return rotation;
  }
  
  //Thresholding, color white if threshold reached, black ortherwise
  PImage applyThresholdBinary(PImage imgRaw) {
    //threshold = (int) (thresholdBar.getPos() * 255);
    //VALUE CHOSEN
    threshold = 120;
    PImage img = createImage(imgRaw.width, imgRaw.height, RGB);
    for(int i = 0; i < img.width * img.height; i++) {
      if (brightness(imgRaw.pixels[i]) <= threshold) {
        img.pixels[i] = color(0); //black
      } else {
        img.pixels[i] = color(255); //white
      }
    }
    return img;
  }
  
  //Thresholding, color black if threshold reached, white ortherwise
  PImage applyThresholdBinaryInverted(PImage imgRaw) {
    //threshold = (int) (thresholdBar.getPos() * 255);
    //VALUE CHOSEN
    threshold = 120;
    PImage img = createImage(imgRaw.width, imgRaw.height, RGB);
    for(int i = 0; i < img.width * img.height; i++) {
      if (brightness(imgRaw.pixels[i]) <= threshold) {
        img.pixels[i] = color(255); //white
      } else {
        img.pixels[i] = color(0); //black
      }
    }
    return img;
  }
  
  PImage applySaturationThreshold(PImage imgRaw) {
    //VALUE CHOSEN  
    threshold = 90;
    PImage img = createImage(imgRaw.width, imgRaw.height, RGB);
    for(int i = 0; i < img.width * img.height; i++) {
      if (saturation(imgRaw.pixels[i]) <= threshold) {
        img.pixels[i] = color(0); //white
      } else {
        img.pixels[i] = color(255); //black
      }
    }
    return img;
  }
  
  //hue algorithm, pixel selection from color range
  PImage applyHue(PImage imgRaw) {
    //hueMin = (int) (hueBarMin.getPos() * 255);
    //hueMax = (int) (hueBarMax.getPos() * 255);
    
    //VALUES CHOSEN
    hueMin = 96;
    hueMax = 136;
    PImage img = createImage(imgRaw.width, imgRaw.height, RGB);
    for(int i = 0; i < img.width * img.height; i++) {
      int hue = (int) hue(imgRaw.pixels[i]);
      if (hue >= hueMin && hue <= hueMax) {
        img.pixels[i] = imgRaw.pixels[i]; //keeps its color
      } else {
        img.pixels[i] = color(0); //set to black
      }
    }
    return img;
  }
  
  //Returns weight of a matrix (1 if 0)
  float returnWeights(float[][] kernel) {
    float weight = 0.f;
    for (int i = 0; i< kernel.length; i++) {
      for (int j = 0; j < kernel[0].length; j++) {
        weight += kernel[i][j];
      }
    }
    return (weight == 0.f) ? 1 : weight;
  }
  
  /**
  * convolute image applying the kernel
  **/
  PImage convolute(PImage img, float[][] kernel) {
    if (kernel == null){
      kernel = new float[][] {{0, 0, 0}, 
                             {0, 2, 0}, 
                             {0, 0, 0}};
    }
    PImage result = createImage(img.width, img.height, ALPHA);
    float weight = returnWeights(kernel);
    
    for (int i = img.width + 1; i < img.width * (img.height - 1) - 1; i++) { //skip first and last line
      if ((i % img.width != 0) && (i % img.width != width - 1)){ //skip first and last column
        float newValue = 0.f;
        for (int ligne = 0; ligne < kernel.length; ligne ++) {
          for (int colonne = 0; colonne < kernel[0].length; colonne++) {
            newValue += brightness(img.pixels[i + (ligne - 1) * img.width + (colonne - 1)]) * kernel[ligne][colonne];      
          }
        }
        result.pixels[i] = color((int)(newValue/weight));
      }
    }
    return result;
  }
  
  //specific convolution : blur
  PImage gaussianBlur(PImage img) {
    float[][] kernel = {{9, 12, 9},
                        {12, 15, 12}, 
                        {9, 12, 9}};
    return convolute(img, kernel);
  }
  
  //specific convolution (double convolution)
  PImage sobel(PImage img) {
    float[][] hKernel = {{0, 1, 0},
                         {0, 0, 0},
                         {0, -1, 0}};
    
    float[][] vKernel = {{0, 0, 0},
                         {1, 0, -1},
                         {0, 0, 0}};
    PImage result = createImage(img.width, img.height, ALPHA);
    // clear the image
    for (int i = 0; i < img.width * img.height; i++) {
      result.pixels[i] = color(0);
    }
    float max=0;
    float[] buffer = new float[img.width * img.height];
    
    // double convolution
    for (int y = 2; y < img.height - 2; y++) {
    // Skip top and bottom edges
      for (int x = 2; x < img.width - 2; x++) {
      // Skip left and right
        float sum_h = 0.f;
        float sum_v = 0.f;
        //hKernel
        for (int ligne = 0; ligne < hKernel.length; ligne ++) {
          for (int colonne = 0; colonne < hKernel[0].length; colonne++) {
            sum_h += img.pixels[(y * img.width + x) + (ligne - 1)*img.width + (colonne - 1)] * hKernel[ligne][colonne];
          }
        }
        //vKernel
        for (int ligne = 0; ligne < vKernel.length; ligne ++) {
          for (int colonne = 0; colonne < vKernel[0].length; colonne++) {
            sum_v += img.pixels[(y * img.width + x) + (ligne - 1)*img.width + (colonne - 1)] * vKernel[ligne][colonne];
          }
        }
        //save sum into buffer
        float sum = sqrt(sum_h * sum_h + sum_v * sum_v);
        buffer[y * img.width + x] = sum;
        //update max
        max = (sum > max) ? sum : max;
      }
    }
    //iterate over the buffer and set pixels over 30% of max
    for (int i = 0; i < buffer.length; i++){
      if (buffer[i] > (int)(max * 0.3f)) {
      // 30% of the max
        result.pixels[i] = color(255);
      } else {
        result.pixels[i] = color(0);
      }
    }
    return result;
  }
  
  //HOUGH
  ArrayList<PVector> hough(PImage edgeImg, int nLines) {
  float discretizationStepsPhi = 0.06f;
  float discretizationStepsR = 2.5f;
      
  ArrayList<Integer> bestCandidates = new ArrayList<Integer>();

  // dimensions of the accumulator
  int phiDim = (int) (Math.PI / discretizationStepsPhi);
  int rDim = (int) (((edgeImg.width + edgeImg.height) * 2 + 1) / discretizationStepsR);
  
  //sin cos precomputation
  if (tabSin == null){
    initSinCosArray(phiDim, discretizationStepsPhi, discretizationStepsR);
  }
 
  // our accumulator (with a 1 pix margin around)
  int[] accumulator = new int[(phiDim + 2) * (rDim + 2)];
  // Fill the accumulator: on edge points (ie, white pixels of the edge
  // image), store all possible (r, phi) pairs describing lines going
  // through the point.
  for (int y = 0; y < edgeImg.height; y++) {
    for (int x = 0; x < edgeImg.width; x++) {
    // Are we on an edge?
      if (brightness(edgeImg.pixels[y * edgeImg.width + x]) != 0) {
        for (int phiIndex = 0; phiIndex < phiDim; phiIndex++){
          int rIndex = (int) (x * tabCos[phiIndex] + y * tabSin[phiIndex] + (rDim - 1) / 2);
          accumulator[(int) ((phiIndex + 1) * (rDim + 2)) + rIndex + 1] += 1;
        }
      }
    }
  }
  
  //DEBUG
  /*
  PImage houghImg = createImage(rDim + 2, phiDim + 2, ALPHA);
  for (int i = 0; i < accumulator.length; i++) {
    houghImg.pixels[i] = color(min(255, accumulator[i]));
  }
  houghImg.updatePixels();
  houghImg.resize(400,400);
  image(houghImg, 0, 0);
  */
  //END DEBUG
    
  //LOCAL MAXIMA
  // size of the region we search for a local maximum
  int neighbourhood = 10;
  // only search around lines with more that this amount of votes
  // (to be adapted to your image)
  int minVotes = 100;
  for (int accR = 0; accR < rDim; accR++) {
    for (int accPhi = 0; accPhi < phiDim; accPhi++) {
    // compute current index in the accumulator
    int idx = (accPhi + 1) * (rDim + 2) + accR + 1;
    if (accumulator[idx] > minVotes) {
      boolean bestCandidate=true;
      // iterate over the neighbourhood
      for(int dPhi=-neighbourhood/2; dPhi < neighbourhood/2+1; dPhi++) {
        // check we are not outside the image
        if( accPhi+dPhi < 0 || accPhi+dPhi >= phiDim) continue;
        for(int dR=-neighbourhood/2; dR < neighbourhood/2 +1; dR++) {// check we are not outside the image
          if(accR+dR < 0 || accR+dR >= rDim) continue;
          int neighbourIdx = (accPhi + dPhi + 1) * (rDim + 2) + accR + dR + 1;
          if(accumulator[idx] < accumulator[neighbourIdx]) {
            // the current idx is not a local maximum!
            bestCandidate=false;
            break;
          }
        }
        if(!bestCandidate) break;
        }
        if(bestCandidate) {
          // the current idx *is* a local maximum
          bestCandidates.add(idx);
        }
      }
    }
  }
  Collections.sort(bestCandidates, new HoughComparator(accumulator));
  ArrayList<PVector> lines = new ArrayList<PVector>();

  //filter accumulator
  int[] filteredAcc = new int[(phiDim + 2 )*(rDim + 2)];
  int nbLines = min(bestCandidates.size(), nLines);
  for (int i = 0; i < nbLines; i++) {
    int idx = bestCandidates.get(i);
    filteredAcc[idx] = accumulator[idx];
  }
    
  //ADD LINES
  for(int idx = 0; idx < filteredAcc.length; idx++) {
    if (filteredAcc[idx] != 0) {
      int accPhi = (int) (idx / (rDim + 2)) - 1;
      int accR = idx - (accPhi + 1) * (rDim + 2) - 1;
      float r = (accR - (rDim - 1) * 0.5f) * discretizationStepsR;
      float phi = accPhi * discretizationStepsPhi;
      lines.add(new PVector(r, phi));
    }
  }
  drawLines(edgeImg.width, lines, nLines);
  getIntersections(lines);
  return lines;
}

ArrayList<PVector> getIntersections(ArrayList<PVector> lines) {
  ArrayList<PVector> intersections = new ArrayList<PVector>();
  for (int i = 0; i < lines.size() - 1; i++) {
      PVector line1 = lines.get(i);
    for (int j = i + 1; j < lines.size(); j++) {
      PVector line2 = lines.get(j);
      // compute the intersection and add it to 'intersections'
      float d = cos(line2.y) * sin(line1.y) - cos(line1.y) * sin(line2.y);
      
      float x = (line2.x * sin(line1.y) - line1.x * sin(line2.y))/d;
      float y = (-line2.x * cos(line1.y) + line1.x * cos(line2.y))/d;
      
      intersections.add(new PVector(x, y));
      // draw the intersection
      fill(255, 128, 0);
      ellipse(x, y, 10, 10);
    }
  }
  return intersections;
}

void initSinCosArray(int phiDim, float discretizationStepsPhi, float discretizationStepsR) {
  //sin/cos in array
  tabSin = new float[phiDim];
  tabCos = new float[phiDim];
  
  float ang = 0;
  float inverseR = 1.f / discretizationStepsR;
  
  for (int accPhi = 0; accPhi < phiDim; ang += discretizationStepsPhi, accPhi++){
    tabSin[accPhi] = (float) sin(ang) * inverseR;
    tabCos[accPhi] = (float) cos(ang) * inverseR;
  } 
}

  void drawLines(int imgWidth, ArrayList<PVector> lines, int nLines) {
    int nbLines = min(lines.size(), nLines);
    for (int i = 0; i < nbLines; i++) {
      int x0 = 0;
      int y0 = (int) (lines.get(i).x / sin(lines.get(i).y));
      int x1 = (int) (lines.get(i).x / cos(lines.get(i).y));
      int y1 = 0;
      int x2 = imgWidth;
      int y2 = (int) (-cos(lines.get(i).y) / sin(lines.get(i).y) * x2 + lines.get(i).x / sin(lines.get(i).y));
      int y3 = imgWidth;
      int x3 = (int) (-(y3 - lines.get(i).x / sin(lines.get(i).y)) * (sin(lines.get(i).y) / cos(lines.get(i).y)));
      // Finally, plot the lines
      stroke(204,102,0);
      if (y0 > 0) {
        if (x1 > 0) {
          line(x0, y0, x1, y1);
        } else if (y2 > 0) {
          line(x0, y0, x2, y2);
        } else {
          line(x0, y0, x3, y3);
        }
      } else {
        if (x1 > 0) {
          if (y2 > 0) {
            line(x1, y1, x2, y2);
          } else {
            line(x1, y1, x3, y3);
          } 
        }else {
          line(x2, y2, x3, y3);
        }
      }
    }
  }
}