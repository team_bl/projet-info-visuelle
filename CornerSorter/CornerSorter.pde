import java.util.List;
import java.util.Collections;
import java.util.Comparator;

class CWComparator implements Comparator<PVector> {
        PVector center;
        public CWComparator(PVector center) {
                this.center = center;
        }

        @Override
        public int compare(PVector b, PVector d) {
                if(Math.atan2(b.y-center.y,b.x-center.x)<Math.atan2(d.y-center.y,d.x-center.x))
                        return -1;
                else
                        return 1;
        }
}

class QuadSorter {
  List<PVector> sortCorners(List<PVector> quad) {
          // Sort
          PVector a = quad.get(0);
          PVector b = quad.get(2);
          PVector center = new PVector((a.x+b.x)/2,(a.y+b.y)/2);
  
          Collections.sort(quad, new CWComparator(center));
  
          PVector orig = new PVector(0, 0);
          PVector closestToOrig = quad.get(0);
          int indexOfClosestToOrig = 0;
          for (int i=0; i < quad.size(); i++) {
                  PVector curCorner = quad.get(i);
                  if (closestToOrig.dist(orig) > curCorner.dist(orig)) {
                          closestToOrig = curCorner;
                          indexOfClosestToOrig = i;
                  }
          }
          Collections.rotate(quad, -indexOfClosestToOrig);
  
          return quad;
  }
}