int scale = 0;
int angleX = 0;
int angleY = 0;

void settings() {
  size(500, 500, P2D);
}

void setup() {
  frameRate(30);
}

void draw() {
  background(255, 255, 255);
  My3DPoint eye = new My3DPoint(-200, -200, -5000);
  My3DPoint origin = new My3DPoint(0, 0, 0);
  My3DBox input3DBox = new My3DBox(origin, 100, 100, 100);
  float ratio = (float) Math.pow(1.1, scale);
  float rotateX = (float) (Math.PI*angleX/30);
  float rotateY = (float) (Math.PI*angleY/30);
  
  input3DBox = transformBox(input3DBox, scaleMatrix(ratio, ratio, ratio));
  input3DBox = transformBox(input3DBox, rotateXMatrix(rotateX));
  input3DBox = transformBox(input3DBox, rotateYMatrix(rotateY));
  projectBox(eye, input3DBox).render();
}

void mouseWheel(MouseEvent e) {
  scale += e.getCount();
}

void mouseDragged() {
  scale += (pmouseY - mouseY)/5;
}

void keyPressed() {
  if (key == CODED) {
    if (keyCode == UP) {
      angleX = (angleX-1)%60;
    }
    if (keyCode == DOWN) {
      angleX = (angleX+1)%60;
    }
    if (keyCode == RIGHT) {
      angleY = (angleY+1)%60;
    }
    if (keyCode == LEFT) {
      angleY = (angleY-1)%60;
    }
  }
}

My2DPoint projectPoint(My3DPoint eye, My3DPoint p) {
  // matrix calculation gives this result - not explicited here
  return new My2DPoint(eye.z * (p.x - eye.x) / (eye.z - p.z),
  eye.z * (p.y - eye.y) / (eye.z - p.z));
}

My2DBox projectBox(My3DPoint eye, My3DBox box) {
  My2DPoint[] ls = new My2DPoint[8];
  for (int i=0; i<8; i++) {
    ls[i] = projectPoint(eye, box.p[i]);
  }
  return new My2DBox(ls);
}

My3DBox transformBox(My3DBox box, float[][] transformMatrix) {
  My3DPoint[] p = new My3DPoint[8];
  for (int i=0; i<8; i++) {
    p[i] = euclidian3DPoint(matrixProduct(transformMatrix, homogeneous3DPoint(box.p[i])));
  }
  return new My3DBox(p);
}